import React, {useState} from 'react';
import Home from './home.js';

export default function renderHomeIndex() {
    const [count, setCount] = useState(0);
    const countClick = () => {
        setCount(count + 1);
    };
    return (
        <Home title="Tao là hào đại ka đang đếm " countClick={() => countClick()} value={count}/>
    );
}
