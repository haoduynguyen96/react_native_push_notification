import React, {useState} from 'react';
import {View, Text, Button} from 'react-native';

export default function renderHomePage(props) {
    //const [count, setCount] = useState(0);
    return (
        <View>
            <Text>
                {props.title + props.value}
            </Text>
            <Button onPress={() => props.countClick()}
                    title="click"
                    color="red"
                    accessibilityLabel="Click this button to increase count"/>
        </View>
    );
}

