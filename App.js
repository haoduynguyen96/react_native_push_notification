/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React,{Component} from 'react';
import 'react-native-gesture-handler';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
import {
    Header,
    LearnMoreLinks,
    Colors,
    DebugInstructions,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {Router, Scene,Stack} from 'react-native-router-flux';
import Home from './src/screen/home/index'
export default class App extends Component {
    getToken = async () => {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        console.log('fcmToken',fcmToken);
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            console.log('fcmToken',fcmToken);
            if (fcmToken) {
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }
    };
    checkPermission = async () => {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            await this.getToken();
        } else {
            await this.requestPermission();
        }
    };
    requestPermission = async () => {
        try {
            await firebase.messaging().requestPermission();
            await this.getToken();
        } catch (error) {
            console.log('permission rejected');
        }
    };
    createNotificationListeners = async () => {
        firebase.notifications().onNotification(async notification => {
            notification.android.setChannelId('insider').setSound('default');
            firebase.notifications().displayNotification(notification);
        });
    };

    async componentDidMount() {
        const channel = new firebase.notifications.Android.Channel('insider', 'insider channel', firebase.notifications.Android.Importance.Max)
        firebase.notifications().android.createChannel(channel);
        await this.checkPermission();
        await this.createNotificationListeners();
    }
    render() {
        return (
            <Router>
                <Stack key="root">
                    <Scene key="home" component={Home} title="home" />
                </Stack>
            </Router>
        );
    }
}
const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter,
    },
    engine: {
        position: 'absolute',
        right: 0,
    },
    body: {
        backgroundColor: Colors.white,
    },
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
        color: Colors.black,
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
        color: Colors.dark,
    },
    highlight: {
        fontWeight: '700',
    },
    footer: {
        color: Colors.dark,
        fontSize: 12,
        fontWeight: '600',
        padding: 4,
        paddingRight: 12,
        textAlign: 'right',
    },
});


